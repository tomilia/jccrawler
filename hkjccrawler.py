#coding=utf-8
#!/usr/bin/python
import urllib2,json
import threading
import datetime
from datetime import timedelta
import sys
import os.path
path="/Users/tomilia/Documents/Workspace/hkjc/"
url='https://bet.hkjc.com/football/getJSON.aspx?jsontype=index.aspx'
fhaurl="https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_fha.aspx"
hhaurl="https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_hha.aspx"
hdcurl="https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_hdc.aspx"
ftsurl="https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_fts.aspx"
crsurl="https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_crs.aspx"
headers={'User-Agent':'Mozilla/5.0 (compatible; MSIE 9.0; AOL 9.7; AOLBuild 4343.19; Windows NT 6.1; WOW64; Trident/5.0; FunWebProducts)'}
request=urllib2.Request(url=url,headers=headers)
fha_request=urllib2.Request(url=fhaurl,headers=headers)
hha_request=urllib2.Request(url=hhaurl,headers=headers)
hdc_request=urllib2.Request(url=hdcurl,headers=headers)
fts_request=urllib2.Request(url=ftsurl,headers=headers)
crs_request=urllib2.Request(url=crsurl,headers=headers)
had_basic=urllib2.urlopen(request)
fha_basic=urllib2.urlopen(fha_request)
#half time 
hha_basic=urllib2.urlopen(hha_request)
#handicap home draw away
hdc_basic=urllib2.urlopen(hdc_request)
#handicap home away
fts_basic=urllib2.urlopen(fts_request)
#half full time
crs_basic=urllib2.urlopen(crs_request)
data = json.load(had_basic)
fha_data = json.load(fha_basic)
hdc_data = json.load(hdc_basic)
hha_data = json.load(hha_basic)
fts_data = json.load(fts_basic)
crs_data = json.load(crs_basic)
mapx={}
date_map={}
now=str(datetime.datetime.now())
def job():
    for obj in fts_data[1]['matches']:
        id=obj['matchID']
        date=obj['matchDate'][:10]
        date_map[date][id]['ftsodds']=[{now:[obj['ftsodds']['H'],obj['ftsodds']['N'],obj['ftsodds']['A']]}]
    for obj in crs_data[1]['matches']:
        id=obj['matchID']
        date=obj['matchDate'][:10]
        date_map[date][id]['crsodds']=[{now:obj['crsodds']}]
def check_if_no_new_date():
    if len(date_map)==0:
        sys.exit(0)
    else:
        return
for obj in range(0,len(data)):
    id=data[obj]['matchID']
    date=data[obj]['matchDate'][:10]
    league=data[obj]['league']['leagueShortName']
    home=data[obj]['homeTeam']['teamNameEN']
    away=data[obj]['awayTeam']['teamNameEN']
    h=[{now:data[obj]['hadodds']['H']}]
    d=[{now:data[obj]['hadodds']['D']}]
    a=[{now:data[obj]['hadodds']['A']}]
    #fha
    if date in date_map.keys():
        pass
    else:
        date_map[date]={}
    date_map[date][id]={'id':id,'date':date,'league':league,'homeName':home,'awayName':away,'home':h,'draw':d,'away':a}
check_if_no_new_date()
for obj in fha_data[1]['matches']:
    id=obj['matchID']
    date=obj['matchDate'][:10]
    date_map[date][id]['fhaodds']=[{now:[obj['fhaodds']['H'],obj['fhaodds']['D'],obj['fhaodds']['A']]}]
for obj in hha_data[1]['matches']:
    id=obj['matchID']
    date=obj['matchDate'][:10]
    date_map[date][id]['hhaodds']=[{now:[obj['hhaodds']['HG']+" "+obj['hhaodds']['H'],obj['hhaodds']['HG'][1]+" "+obj['hhaodds']['D'],obj['hhaodds']['AG']+" "+obj['hhaodds']['A']]}]
for obj in hdc_data[1]['matches']:
    id=obj['matchID']
    date=obj['matchDate'][:10]
    date_map[date][id]['hdcodds']=[{now:[obj['hdcodds']['HG']+" "+obj['hdcodds']['H'],obj['hdcodds']['AG']+" "+obj['hdcodds']['A']]}]

t = threading.Thread(target = job)

# 執行該子執行緒
t.start()
t.join()
pr=[]
for dates in date_map:
    for key,value in date_map[dates].items():
        pr.append(value)
    f = open(path+str(dates)[:7]+"_aps.json", "w+")
    f.write(json.dumps(pr))
    f.close()
    pr=[]
    




#fh=fha_data[1]['matches'][obj]['fhaodds']['H']
#fd=fha_data[1]['matches'][obj]['fhaodds']['D']
#fa=fha_data[1]['matches'][obj]['fhaodds']['A']
#for distro in distros_dict:
 #   print(distro['Name'])