#coding=utf-8
#!/usr/bin/python
import urllib2,json
import threading
import datetime
from datetime import timedelta
class jcdownloader:
    def __init__(self):
        self.mapx={}
        now=str(datetime.datetime.now())
        self.url='https://bet.hkjc.com/football/getJSON.aspx?jsontype=index.aspx'
        self.fhaurl="https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_fha.aspx"
        self.hhaurl="https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_hha.aspx"
        self.hdcurl="https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_hdc.aspx"
        self.ftsurl="https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_fts.aspx"
        self.crsurl="https://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_crs.aspx"
        self.headers={'User-Agent':'Mozilla/5.0 (compatible; MSIE 9.0; AOL 9.7; AOLBuild 4343.19; Windows NT 6.1; WOW64; Trident/5.0; FunWebProducts)'}
        request=urllib2.Request(url=self.url,headers=self.headers)
        had_basic=urllib2.urlopen(request)
        data = json.load(had_basic)
        for obj in range(0,len(data)):
            id=data[obj]['matchID']
            id_item=data[obj]['matchIDinofficial']
            match_date=data[obj]['matchTime']
            league=data[obj]['league']['leagueShortName']
            home=data[obj]['homeTeam']['teamNameEN']
            away=data[obj]['awayTeam']['teamNameEN']
            h=[{now:data[obj]['hadodds']['H']}]
            d=[{now:data[obj]['hadodds']['D']}]
            a=[{now:data[obj]['hadodds']['A']}]
            hasBegun=data[obj]['liveEvent']['matchstate']
            #fha
            self.mapx[id]=({'id_item':id_item,'date':match_date,'id':id,'league':league,'homeName':home,'awayName':away,'home':h,'draw':d,'away':a,'hasBegun':hasBegun,'newMatch':True})
        #half time 
        fha_request=urllib2.Request(url=self.fhaurl,headers=self.headers)
        fha_basic=urllib2.urlopen(fha_request)
        fha_data = json.load(fha_basic)
        for obj in fha_data[1]['matches']:
            id=obj['matchID']
            self.mapx[id]['fhaodds']=[{now:[obj['fhaodds']['H'],obj['fhaodds']['D'],obj['fhaodds']['A']]}]
        #handicap home draw away
        hha_request=urllib2.Request(url=self.hhaurl,headers=self.headers)
        hha_basic=urllib2.urlopen(hha_request)
        hha_data = json.load(hha_basic)
        for obj in hha_data[1]['matches']:
            id=obj['matchID']
            self.mapx[id]['hhaodds']=[{now:[obj['hhaodds']['HG']+" "+obj['hhaodds']['H'],obj['hhaodds']['HG'][1]+" "+obj['hhaodds']['D'],obj['hhaodds']['AG']+" "+obj['hhaodds']['A']]}]
        hdc_request=urllib2.Request(url=self.hdcurl,headers=self.headers)
        hdc_basic=urllib2.urlopen(hdc_request)
        #handicap home away
        hdc_data = json.load(hdc_basic)
        for obj in hdc_data[1]['matches']:
            id=obj['matchID']
            self.mapx[id]['hdcodds']=[{now:[obj['hdcodds']['HG']+" "+obj['hdcodds']['H'],obj['hdcodds']['AG']+" "+obj['hdcodds']['A']]}]
        fts_request=urllib2.Request(url=self.ftsurl,headers=self.headers)
        fts_basic=urllib2.urlopen(fts_request)
        #half full time
        fts_data = json.load(fts_basic)
        for obj in fts_data[1]['matches']:
            id=obj['matchID']
            self.mapx[id]['ftsodds']=[{now:[obj['ftsodds']['H'],obj['ftsodds']['N'],obj['ftsodds']['A']]}]
        crs_request=urllib2.Request(url=self.crsurl,headers=self.headers)
        crs_basic=urllib2.urlopen(crs_request)
        crs_data = json.load(crs_basic)
        for obj in crs_data[1]['matches']:
            id=obj['matchID']
            self.mapx[id]['crsodds']=[{now:obj['crsodds']}]
    def getOdds(self,com_id,values,item_name):
        now=str(datetime.datetime.now())
        if(self.mapx[com_id][item_name][0].values()[0]!=values[-1].values()[0]):
            values.append({now:self.mapx[com_id][item_name][0].values()[0]})
        
        self.mapx[com_id]["newMatch"]=False
        return values
    def getNewMatches(self):
        all_current_match_list=[]
        for matches in self.mapx:
            all_current_match_list.append(matches)
        return all_current_match_list
    def getNewOdds(self,com_id):
        return self.mapx[com_id]
    def getCurrentState(self,com_id):
        try:
            #has live event or begun
            if(self.mapx[com_id]['hasBegun']!=None and self.mapx[com_id]['hasBegun']!="BeforeKickOff"):
                print("abc:"+" "+self.mapx[com_id]['homeName']+" "+self.mapx[com_id]['hasBegun'])
                return True
            #does not have
            else:
                print("def:"+" "+self.mapx[com_id]['homeName']+" "+self.mapx[com_id]['hasBegun'])
                return False
        except:
            return False